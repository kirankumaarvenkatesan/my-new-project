package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Given;
import wdMethods.ProjectMethods;

public class MyLeadsPage extends ProjectMethods {

	public MyLeadsPage() {
		
		PageFactory.initElements(driver, this);
		
	}
	
	@FindBy(xpath="//a[.='Create Lead']")
	WebElement createleadbutton;
	
	@FindBy(xpath="//a[.='Merge Leads']")
	WebElement mergeleadbutton;
	
	@FindBy(xpath="//a[text()='Find Leads']")
	WebElement findleads;
	
	@Given("Click on CreateLead")
	public CreateLeadPage clickcreatelead() {
		click(createleadbutton);
		return new CreateLeadPage();
		
	}
	@Given("Click on MergeLeads")
	public MergeLeadPage clickmergelead() {
	   click(mergeleadbutton);	
	   return new MergeLeadPage();
	   
	}
	@Given("Click on FindLeads")
	public FindLeadsMainPage clickfindlead() {
		
		click(findleads);
		return new FindLeadsMainPage();
		
		
	}
	
	
	
	
	
}
