package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import wdMethods.ProjectMethods;

public class CreateLeadPage extends ProjectMethods {

	public CreateLeadPage() {
		PageFactory.initElements(driver,this);
		
	}
	
	@FindBy(xpath="//input[@name='companyName'][@class='inputBox']")
	private WebElement companynameTF;
	
	@FindBy(xpath="//input[@name='firstName'][@class='inputBox']")
	private WebElement firstnameTF;
	
	@FindBy(xpath="//input[@name='lastName'][@class='inputBox']")
	private WebElement lastnameTF;
	
	@FindBy(xpath="//input[@value='Create Lead']")
	private WebElement createleadbutton;
	
	@Given("Enter the CompanyName as (.*)")
	public CreateLeadPage entercompanyname(String companyname) {
		type(companynameTF, companyname);
		return new CreateLeadPage();
	}
	@Given("Enter the FirstName as (.*)")
	public CreateLeadPage enterfirstname(String firstname) {
		type(firstnameTF, firstname);
		return new CreateLeadPage();
	}
	@Given("Enter the LastName as (.*)")
	public CreateLeadPage enterlastname(String lastname) {
		type(lastnameTF, lastname);
		return new CreateLeadPage();
	}
	@When("Click on CreateLeadButton")
	public ViewLeadPage clickcreatelead() {
		click(createleadbutton);
		return new ViewLeadPage();
		
	}
	
	
	
	
	
	
}
