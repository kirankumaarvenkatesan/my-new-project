package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import wdMethods.ProjectMethods;

public class ViewLeadPage extends ProjectMethods {

	public ViewLeadPage() {
		
		PageFactory.initElements(driver, this);
	}
	@FindBy(xpath="//span[@id='viewLead_firstName_sp']")
	private WebElement firstnametext;
	
	@FindBy(xpath="//a[text()='Edit']")
	private WebElement editbutton;
	
	@FindBy(xpath="//div[.='View Lead']/following::div[.='Lead']/following::table//span[.='Company Name']/../../td[2]/span")
	private WebElement actualcompanynametext;
	
	@FindBy(xpath="//a[.='Delete']")
	private WebElement deleteButton;
	
	@Then("Verify the FirstName is (.*)")
	public void verifyfirstnametext(String expectedText) {
		
		String actualFN=getText(firstnametext);
		verifyExactText(firstnametext, expectedText); 
		
	}
	@Then("Verify the new company name as (.*)")
	public ViewLeadPage verifycompanynametext(String expectedcompanynametext) {
		
		verifyPartialText(actualcompanynametext, expectedcompanynametext);
		return new ViewLeadPage();
		
	}
	
	@Given("Click on Edit Button")
	public EditLeadPage clickeditbutton() {
		
		click(editbutton);
		return new EditLeadPage();
	}
	
     public MyLeadsPage clickdeletebutton() {
		
		click(deleteButton);
		return new MyLeadsPage();
	}
	
	
	
	
}
