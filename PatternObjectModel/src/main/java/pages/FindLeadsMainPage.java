package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Given;
import wdMethods.ProjectMethods;

public class FindLeadsMainPage extends ProjectMethods {

	public FindLeadsMainPage() {
		
		PageFactory.initElements(driver,this);
		
	}
	
	@FindBy(xpath="//div[.='Find Leads']/following::label[.='First name:']/../div/input")
	private WebElement firstnameTF;
	
	@FindBy(xpath="//button[text()='Find Leads']")
	private WebElement findleadsbutton;
	
	@FindBy(xpath="//span[.='Lead List']/following::table[2]//tr[1]/td[1]//a")
	private WebElement firstrowID;
	
	@FindBy(xpath="(//span[.='Find by']/following::div//span[.='Phone'])[2]")
	private WebElement phoneTab;
	
	@FindBy(xpath="//input[@name='phoneNumber']")
	private WebElement phoneNumberTF;
	
	
	
	@FindBy(xpath="//input[@name='id']")
	private WebElement leadIDTF;
	
	@FindBy(xpath="(//span[.='Lead List']/following::table/following::div)[8]")
	private WebElement errmsg;

	public static String firstrowLeadIDText="";
	
	@Given("Enter the Firstname as (.*)")
	public FindLeadsMainPage enterfirstname(String fn) {
		
		type(firstnameTF,fn);
		
		return new FindLeadsMainPage();
		
	}
	
	@Given("Click on FindLeadsbutton")
	public FindLeadsMainPage clickfindleadsbutton() {
		
		click(findleadsbutton);
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		firstrowLeadIDText=firstrowLeadIDText+getText(firstrowID);
		
		return new FindLeadsMainPage();
	}
	
	/*@Given("Click on the first row Lead")
	public ViewLeadPage clickfirstrowid() {
		
		click(firstrowID);
		return new ViewLeadPage();
		
	}*/
	
	public FindLeadsMainPage clickphoneTab() {
		
		click(phoneTab);
		return new FindLeadsMainPage();
		
		
	}
	public FindLeadsMainPage enterphonenumber(String phno) {
		type(phoneNumberTF, phno);
		return new FindLeadsMainPage();
		
	}
	
	/*public FindLeadsMainPage clickonfindleadsbutton() {
		
		click(findleadsbutton);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return new FindLeadsMainPage();
        
        
	}*/
	
	public ViewLeadPage clickonfirstrowID() {
		
		
		click(firstrowID);
		return new ViewLeadPage();
		
	}
	
	
	
	public FindLeadsMainPage enterleadidtext(String leadidtext) {
		
		type(leadIDTF, leadidtext);
		return new FindLeadsMainPage();
		
	}
	
	public FindLeadsMainPage verifyerrmsg() {
		
		verifyDisplayed(errmsg);
		return new FindLeadsMainPage();
		
	}
	
	
	
	
	
	
}
