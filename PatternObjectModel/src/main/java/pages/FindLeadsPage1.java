package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Given;
import wdMethods.ProjectMethods;

public class FindLeadsPage1 extends ProjectMethods{

	public FindLeadsPage1() {
		
		PageFactory.initElements(driver,this);
	}
	
	@FindBy(xpath="//input[@name='id']")
    WebElement leadID1;
	
	@FindBy(xpath="//button[.='Find Leads']")
	WebElement findleadsbutton1;
	
	@FindBy(xpath="//table[@class='x-grid3-row-table']//tr[1]/td[1]/div/a")
	WebElement firstrowlead1;
	
	@Given("Enter the LeadidA as (.*)")
	public FindLeadsPage1 enterleadID1(String leadid1text) {
		
		type(leadID1, leadid1text);
		return new FindLeadsPage1();
	}
	@Given("Click on FindleadsA")
	public FindLeadsPage1 clickfindleads() {
		
		click(findleadsbutton1);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return new FindLeadsPage1();
	}
	@Given("Click on FirstRowLeadsA")
	public MergeLeadPage clickfirstrowlead() {
		
		click(firstrowlead1);
	    switchToWindow(0);
		return new MergeLeadPage();
	}
	
	
	
	
	
	
	
	
}
