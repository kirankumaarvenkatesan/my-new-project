package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Given;
import wdMethods.ProjectMethods;

public class MyHomePage extends ProjectMethods{

	public MyHomePage() {
		
		PageFactory.initElements(driver,this);
		
	}
	
	@FindBy(xpath="//a[.='Leads']")
	WebElement leadsbutton;
	
	@Given("Click on Leads")
	public MyLeadsPage clickleadsbutton() {
		
		click(leadsbutton);
		return new MyLeadsPage();
		
		
	}
	
	
	
	
}
