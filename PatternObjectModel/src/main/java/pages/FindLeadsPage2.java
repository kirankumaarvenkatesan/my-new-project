package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Given;
import wdMethods.ProjectMethods;

public class FindLeadsPage2 extends ProjectMethods {
	
	public FindLeadsPage2() {
		
		PageFactory.initElements(driver,this);
	}
	
	@FindBy(xpath="//input[@name='id']")
	WebElement leadID2;
	
	@FindBy(xpath="//button[.='Find Leads']")
	WebElement findleadsbutton2;
	
	@FindBy(xpath="//table[@class='x-grid3-row-table']//tr[1]/td[1]/div/a")
	WebElement firstrowID2;
	
	@Given("Enter the LeadidB as (.*)")
	public FindLeadsPage2 enterleadid1(String leadid2text) {
		
		type(leadID2, leadid2text);
		return new FindLeadsPage2();
	}
	
	@Given("Click on FindleadsB")
	public FindLeadsPage2 clickfindleadsbutton2() {
		
		click(findleadsbutton2);
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return new FindLeadsPage2();
		
	}
	
	@Given("Click on FirstRowLeadsB")
	public MergeLeadPage clickfirstrowid2() {
		
		click(firstrowID2);
		switchToWindow(0);
		
		return new MergeLeadPage();
		
	}
	
	
	
	

}
