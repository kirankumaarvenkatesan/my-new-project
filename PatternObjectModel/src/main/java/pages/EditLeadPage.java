package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Given;
import wdMethods.ProjectMethods;

public class EditLeadPage extends ProjectMethods {

	public EditLeadPage() {
		PageFactory.initElements(driver, this);
		
	}
	
	@FindBy(xpath="//input[@id='updateLeadForm_companyName']")
	private WebElement companynameTF;
	
	@FindBy(xpath="//input[@value='Update']")
	private WebElement updatebutton;
	
	@Given("Edit the Company name as (.*)")
	public EditLeadPage entercompanynametext(String companynametext) {
		
		type(companynameTF,companynametext);
		return new EditLeadPage();
	}
	
	@Given("Click on Update button")
	public ViewLeadPage clickupdateleadbutton() {
		
		click(updatebutton);
		return new ViewLeadPage();
	}
	
	
}
