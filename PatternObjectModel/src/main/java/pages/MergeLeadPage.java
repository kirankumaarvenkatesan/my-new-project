package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Given;
import wdMethods.ProjectMethods;

public class MergeLeadPage extends ProjectMethods{

	public MergeLeadPage() {
		
		PageFactory.initElements(driver,this);
	}
	@FindBy(xpath="//table[@name='ComboBox_partyIdFrom']/following::img[@alt='Lookup'][1]")
	private WebElement lookup1;
	
	@FindBy(xpath="//table[@name='ComboBox_partyIdFrom']/following::img[@alt='Lookup'][2]")
	private WebElement lookup2;
	
	@FindBy(xpath="//a[.='Merge']")
	WebElement mergeleadbutton;	
	
	@Given("Click on lookupA")
	public FindLeadsPage1 clicklookup1() {
		
		click(lookup1);
		switchToWindow(1);
		return new FindLeadsPage1();
			
	}
	@Given("Click on lookupB")
	public FindLeadsPage2 clicklookup2() {
		click(lookup2);
		switchToWindow(1);
        return new FindLeadsPage2();		
	}
	
	@Given("Click on MergeLeadButton")
	public ViewLeadPage clickmergeleadbutton() {
		
		click(mergeleadbutton);
		
		acceptAlert();
		return  new ViewLeadPage();
	}
	
	
	
}
