package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.HomePage;
import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC002_CreateLeadTest extends ProjectMethods{
	@BeforeTest
	public void setData() {
		testCaseName = "TC002_CreateL";
		testDescription = "CreateLead";
		authors = "kiran";
		category = "sanity";
		dataSheetName = "TC002_CreateLead";
		testNodes = "Leads";
	}
	@Test(dataProvider="fetchData")
	public void createlead(String un, String pw, String firstname, String companyname, String lastname) {
		new LoginPage()
		.enterUserName(un)
		.enterPassword(pw)
		.clickLogin()
		.clickcrmsfa()
		.clickleadsbutton()
		.clickcreatelead()
		.entercompanyname(companyname)
		.enterfirstname(firstname)
		.enterlastname(lastname)
		.clickcreatelead()
		.verifyfirstnametext(firstname);
		
		
		
		
	 
		
		
		
		
		
		
		
	}
	
	
}
