/*package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC004_EditLeadTest extends ProjectMethods{

	@BeforeTest
	public void setData() {
		testCaseName = "TC002_EditL";
		testDescription = "EditLead";
		authors = "kiran";
		category = "Smoke";
		dataSheetName = "TC003_EditLead";
		testNodes = "Leads";
	}
	
	@Test
	public void editlead(String un, String pw, String fn, String companynametext, String expectedcompanynametext) {
		
		new LoginPage()
		
		.enterUserName(un)
		
		.enterPassword(pw)
		
		.clickLogin()
		
		.clickcrmsfa()
		
		.clickleadsbutton()
		
		.clickfindlead()
		
		.enterfirstname(fn)
		
		.clickfirstrowid()
		
		.clickeditbutton()
		
		.entercompanynametext(companynametext)
		
		.clickupdateleadbutton()
		
		.verifycompanynametext(expectedcompanynametext);
		
	}
	
	
}
*/