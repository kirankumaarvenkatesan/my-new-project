package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.FindLeadsMainPage;
import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC005_DeleteLeadTest extends ProjectMethods {

	@BeforeTest
	public void setData() {
		testCaseName = "TC002_DeleteL";
		testDescription = "DeleteLead";
		authors = "kiran";
		category = "sanity";
		dataSheetName = "TC002_deleteLead";
		testNodes = "Leads";
	}
	
	@Test(dataProvider="fetchData")
	public void deletelead(String un,String pw, String phno) {
		
		new LoginPage()
		.enterUserName(un)
		.enterPassword(pw)
		.clickLogin()
		.clickcrmsfa()
		.clickleadsbutton()
		.clickfindlead()
		.clickphoneTab()
		.enterphonenumber(phno)
		.clickfindleadsbutton()
		.clickonfirstrowID()
		.clickdeletebutton()
		.clickfindlead()
		.enterleadidtext(FindLeadsMainPage.firstrowLeadIDText)
		.clickfindleadsbutton()
		.verifyerrmsg();
		
		
		
	}
	
}
