package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC003_MergeLead extends ProjectMethods{

	@BeforeTest
	public void setData() {
		testCaseName = "TC002_MergeL";
		testDescription = "MergeLead";
		authors = "kiran";
		category = "Regression";
		dataSheetName = "TC003_MergeLead";
		testNodes = "Leads";
	}
	@Test(dataProvider="fetchData")
	public void mergelead(String un, String pw, String leadid1text, String leadid2text) {
		
		new LoginPage()
		
		.enterUserName(un)
		
		.enterPassword(pw)
		
		.clickLogin()
		
		.clickcrmsfa()
		
		.clickleadsbutton()
		
		.clickmergelead()
		
		.clicklookup1()
		
		.enterleadID1(leadid1text)
		
		.clickfindleads()
		
        .clickfirstrowlead()
        
        .clicklookup2()
        
        .enterleadid1(leadid2text)
        
        .clickfindleadsbutton2()
        
        .clickfirstrowid2()
        
        .clickmergeleadbutton();
        
        
		
		
		
		
		
	}
	
	
	
	
	
	
	
}
