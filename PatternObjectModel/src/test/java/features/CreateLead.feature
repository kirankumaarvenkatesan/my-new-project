Feature: Create Lead for Leaftaps

#Background:
#Given open the browser
#And Max the browser
#And Set the Timeout
#And Launch the URL
Scenario Outline: Create Lead

And Enter the UserName as <userName>
And Enter the Password as <Password>
And Click on Login Button
And Click on CRM/SFA
And Click on Leads
And Click on CreateLead
And Enter the CompanyName as <companyName> 
And Enter the FirstName as <firstName>
And Enter the LastName as <lastName>
When Click on CreateLeadButton
Then Verify the FirstName is <firstName> 
 
 Examples:
|userName|Password|companyName|firstName|lastName|
|DemoSalesManager|crmsfa|verizon|kirankumaar|venkatesan|
|DemoSalesManager|crmsfa|Tcs|ramesh|venkat|

