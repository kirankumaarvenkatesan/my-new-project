Feature: Delete Lead for Leaftaps

Scenario Outline: Edit Lead
Given Enter the UserName as <userName>
And Enter the Password as <passWord> 
And Click on Login Button
And Click on CRM/SFA
And Click on Leads
And Click on FindLeadsSideBar Button
And Click on PhoneTab
And Enter the PhoneNumber as <PhoneNumber>
And Click on FindLeads Button
And Click on FirstRowLeadID
And Click on DeleteLeadButton
And Click on FindLeadsSideBar Button
And Enter the actualfirstrowID in LeadIDTF
And Click on FindLeads Button
Then Verify the Errormessage

Examples:

|userName|passWord|PhoneNumber|
|DemoSalesManager|crmsfa|7845328056|
|DemoSalesManager|crmsfa|9789132927|
