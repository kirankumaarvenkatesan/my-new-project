Feature: Edit Lead for Leaftaps

Scenario Outline: Edit Lead
Given Enter the UserName as <userName>
And Enter the Password as <passWord> 
And Click on Login Button
And Click on CRM/SFA
And Click on Leads
And Click on FindLeads 
And Enter the Firstname as <firstName>
And Click on FindLeadsbutton
And Click on the first row Lead 
And Click on Edit Button 
And Edit the Company name as <CompanyName> 
And Click on Update button 
Then Verify the new company name as <CompanyName> 

Examples:

|userName|passWord|firstName|CompanyName|
|DemoSalesManager|crmsfa|kumaar|uber|
|DemoSalesManager|crmsfa|kumaar|zycus| 

