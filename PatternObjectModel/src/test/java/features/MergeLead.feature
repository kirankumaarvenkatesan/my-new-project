Feature: Merge Lead for LeafTaps

Scenario Outline: Merge Lead

Given Enter the UserName as <userName>
And Enter the Password as <Password>
And Click on Login Button
And Click on CRM/SFA
And Click on Leads
And Click on MergeLeads
And Click on lookupA
And Enter the LeadidA as <LeadIDA> 
And Click on FindleadsA 
And Click on FirstRowLeadsA 
And Click on lookupB 
And Enter the LeadidB as <LeadIDB> 
And Click on FindleadsB
And Click on FirstRowLeadsB
And Click on MergeLeadButton

Examples:
 |userName|Password|LeadIDA|LeadIDB|
 |DemoSalesManager|crmsfa|10096|10097|
 |DemoSalesManager|crmsfa|10100|10101|

