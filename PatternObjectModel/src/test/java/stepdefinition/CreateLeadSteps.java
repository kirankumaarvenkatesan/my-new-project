/*package stepdefinition;

import java.util.concurrent.TimeUnit;

import org.apache.xmlbeans.impl.xb.xsdschema.Public;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateLeadSteps {
	public WebDriver driver;
	@Given("open the browser")
	public void openTheBrowser() {
	    
	System.setProperty("webdriver.chrome.driver",".\\drivers\\chromedriver.exe");
	driver=new ChromeDriver();

	}

	@Given("Max the browser")
	public void maxTheBrowser() {
      driver.manage().window().maximize();	    
	}

	@Given("Set the Timeout")
	public void setTheTimeout() {
	    driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
	}

	@Given("Launch the URL")
	public void launchTheURL() {
	    driver.get("http://leaftaps.com/opentaps/control/main");
	}

	@Given("Enter the UserName as (.*)")
	public void enterTheUserNameAsDemoSalesManager(String un) {
	    
		driver.findElement(By.xpath("//input[@id='username']")).sendKeys(un);
		
	}

	@Given("Enter the Password as (.*)")
	public void enterThePasswordAsCrmsfa(String pw) {
	    
		driver.findElement(By.xpath("//input[@id='password']")).sendKeys(pw);
	}

	@Given("Click on Login Button")
	public void clickOnLoginButton() {
	    
		driver.findElement(By.xpath("//input[@value='Login']")).click();
	}

	@Given("Click on CRM\\/SFA")
	public void clickOnCRMSFA() {
	    
		driver.findElement(By.xpath("//a[contains(.,'CRM/SFA')]")).click();
		
	}

	@Given("Click on Leads")
	public void clickOnLeads() {
	    driver.findElement(By.xpath("//a[.='Leads']")).click();
	}

	@Given("Click on CreateLead")
	public void clickOnCreateLead() {
	    driver.findElement(By.xpath("//a[.='Create Lead']")).click();
		
	}

	@Given("Enter the CompanyName as (.*)")
	public void enterTheCompanyName(String cn) {
	    driver.findElement(By.xpath("//input[@id='createLeadForm_companyName']")).sendKeys(cn);
	}

	@Given("Enter the FirstName as (.*)")
	public void enterTheFirstName(String fn) {
	    
		driver.findElement(By.xpath("//input[@id='createLeadForm_firstName']")).sendKeys(fn);
	}

	@Given("Enter the LastName as (.*)")
	public void enterTheLastName(String ln) {
	    
		driver.findElement(By.xpath("//input[@id='createLeadForm_lastName']")).sendKeys(ln);
	}
	@When("Click on CreateLeadButton")
	public void clickOnCreateLeadButton() {
	    driver.findElement(By.xpath("//input[@value='Create Lead']")).click();
	}

	@Then("Verify the FirstName")
	public void verifyTheFirstName() {
	    
		System.out.println("CreateLead Verified");
		
	}


}
*/